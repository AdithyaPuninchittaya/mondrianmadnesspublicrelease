# About

This repository contains the source code for FRC 5813's submission to the Mondrian Madness minibot challenge, presented
by BAE Systems.

# Developing

While connected to the internet, run  
`gradlew downloadDepsPreemptively`  
to fetch any dependencies you don't already have in your gradle cache. Once this is complete, you can run your code with
the robot by connecting to its WiFi network and running  
`gradlew simulateJava`.

Something to note is that our code does not run directly on the robot; instead, it runs in simulation mode on your PC
and puppets the Romi over the network.

Our Romi has what appears to be an intermittent hardware defect that causes it to continue turning in a circle after it
has been disabled. Turning the robot off and back on appears to temporarily resolve the issue.

# Credits

This project was created using the WPILib java "Romi - Command Robot" template and the implementation of trajectory
execution followed the
[WPILIB Trajectory Tutorial](https://docs.wpilib.org/en/stable/docs/software/examples-tutorials/trajectory-tutorial/index.html).
Tuning for trajectory execution was influenced by
<https://raw.githubusercontent.com/bb-frc-workshops/romi-examples/main/romi-trajectory-ramsete/src/main/java/frc/robot/Constants.java>.
