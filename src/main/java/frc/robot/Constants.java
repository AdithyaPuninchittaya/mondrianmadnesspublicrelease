// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final class RomiDrivetrain {
        /*
         * The following values were determined using frc-characterization.exe:
         * ksVolts = 2.45
         * kvVoltSecondsPerMeter = 3.55
         * kaVoltSecondsSquaredPerMeter = 0.00875
         * kPDriveVel = 0.000661
         *
         * After testing the robot using these values, the performance was not
         * ideal. The values we are currently using were pulled directly from
         * the Romi trajectory example on GitHub, available here:
         * https://raw.githubusercontent.com/bb-frc-workshops/romi-examples/main/romi-trajectory-ramsete/src/main/java/frc/robot/Constants.java
         *
         * We would like to emphasize that the only things that we took from
         * the example are the presently used values for the constants listed
         * above.
         */
        public static final double ksVolts = 0.929;
        public static final double kvVoltSecondsPerMeter = 6.33;
        public static final double kaVoltSecondsSquaredPerMeter = 0.0389;

        public static final double kPDriveVel = 0.085;

        public static final double kTrackwidthMeters = 0.141;
        public static final DifferentialDriveKinematics kDriveKinematics =
                new DifferentialDriveKinematics(kTrackwidthMeters);

        public static final double kMaxSpeedMetersPerSecond = 0.55;
        public static final double kMaxAccelerationMetersPerSecondSquared = 0.275;

        public static final double kTrajectoryMaxVoltage = 8.0;

        // From tutorial, seems to work well on the Romi
        public static final double kRamseteB = 2;
        public static final double kRamseteZeta = 0.7;
    }
}
